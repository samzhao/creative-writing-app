import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import { hashHistory } from 'react-router'
import { routerMiddleware, push } from 'react-router-redux'
import createLogger from 'redux-logger'
import rootReducer from '../reducers'
import forEach from 'lodash/forEach'

import { fromJS, Map } from 'immutable'
import { EditorState, convertToRaw, convertFromRaw } from 'draft-js'

import { persistStore, autoRehydrate, createTransform } from 'redux-persist'

import * as editorActions from '../actions/editor'

const actionCreators = {
  ...editorActions,
  push,
}

const logger = createLogger({
  logger: {log: () => {}},
  level: 'log',
  collapsed: true
})

const router = routerMiddleware(hashHistory)

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
  actionCreators
}) : compose

const persistTransformer = createTransform(
  (inboundState, key) => {
    const state = inboundState

    switch(key) {
      case 'characters':
        return state.toArray()
      case 'ideas':
        return state.toArray()
      case 'editor':
        const rawContent = convertToRaw(state.get('editorState').getCurrentContent())

        return {
          editorState: rawContent,
          suggestions: state.get('suggestions').toArray()
        }
      case 'routing':
        return
      default:
        return state
    }
  },
  (outboundState, key) => {
    const state = outboundState

    switch(key) {
      case 'characters':
        return fromJS(state)
      case 'ideas':
        return fromJS(state)
      case 'editor':
        const rawContent = state.editorState
        forEach(rawContent.entityMap, function(value, key) {
          value.data.mention = fromJS(value.data.mention)
        })
        return Map({
          editorState: EditorState.createWithContent(convertFromRaw(rawContent)),
          suggestions: fromJS(state.suggestions),
          isModalVisible: state.isModalVisible
        })
      case 'routing':
        return
      default:
        return state
    }
  }
)

const enhancer = composeEnhancers(
  applyMiddleware(thunk, router, logger),
  autoRehydrate()
)

export default function configureStore(initialState: Object) {
  const store = createStore(rootReducer, initialState, enhancer)

  if (module.hot) {
    module.hot.accept('../reducers', () =>
      store.replaceReducer(require('../reducers')) // eslint-disable-line global-require
    )
  }

  persistStore(store, {
    keyPrefix: 'writingApp:',
    transforms: [persistTransformer]
  })
  return store
}
