// @flow
import React from 'react';
import { Route, IndexRoute, IndexRedirect } from 'react-router';
import App from './containers/App';
import HomePage from './containers/HomePage';
import IdeasPage from './containers/IdeasPage';
import CharactersPage from './containers/CharactersPage'
import CharacterPage from './containers/CharacterPage'

export default (
  <Route path="/" component={App}>
    <IndexRedirect to="/home" />
    <Route path="/home" component={HomePage}>
      <Route path="/ideas" name="ideas" component={IdeasPage} />
      <Route path="/characters" name="characters" component={CharactersPage}>
        <Route path=":id" component={CharacterPage} />
      </Route>
    </Route>
  </Route>
);
