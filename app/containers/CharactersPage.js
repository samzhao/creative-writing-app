import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Characters from '../components/Characters';
import * as CharactersActions from '../actions/characters';

function mapStateToProps(state) {
  return {
    editor: state.editor,
    characters: state.characters
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(CharactersActions, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Characters)
