import React, { Component, PropTypes } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as EditorActions from '../actions/editor'

import Home from '../components/Home'

function mapStateToProps(state) {
  return {
    editor: state.editor,
    characters: state.characters,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(EditorActions, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)
