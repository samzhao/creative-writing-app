import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Ideas from '../components/Ideas';
import * as IdeaActions from '../actions/idea';
import * as EditorActions from '../actions/editor';

function mapStateToProps(state) {
  return {
    ideas: state.ideas,
    editor: state.editor
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: {
      editorActions: bindActionCreators(EditorActions, dispatch),
      ideaActions: bindActionCreators(IdeaActions, dispatch)
    }
  }
  // return bindActionCreators(Object.assign({}, IdeaActions, EditorActions), dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Ideas);
