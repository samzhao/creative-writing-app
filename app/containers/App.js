// @flow
import React, { Component, PropTypes } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as EditorActions from '../actions/editor'

function mapStateToProps(state) {
  return {
    editor: state.editor
  }
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators(EditorActions, dispatch)
}

class App extends Component {
  static propTypes = {
    children: PropTypes.element.isRequired
  }

  static contextTypes = {
    router: PropTypes.object.isRequired
  }

  onKeyDown(e) {
    const { editor, toggleHelp } = this.props

    switch (e.keyCode) {
      case 27:
        if (this.props.location.pathname !== '/home') {
          this.context.router.push('/')
        }

        if (editor.get('isModalVisible')) {
          toggleHelp()
        }
        break;
      case 67:
        if (e.ctrlKey) {
          if (this.props.location.pathname !== '/characters') {
            this.context.router.push('/characters')
          } else {
            this.context.router.push('/')
          }
        }
        break;

      case 73:
        if (e.ctrlKey) {
          if (this.props.location.pathname !== '/ideas') {
            this.context.router.push('/ideas')
          } else {
            this.context.router.push('/')
          }
        }
        break;

      case 191:
        if (e.ctrlKey) {
          toggleHelp()
        }
      break;
    }
  }

  componentDidMount() {
    const node = document

    if (!node) return

    node.addEventListener('keydown', this.onKeyDown.bind(this))
  }

  componentWillUnmount() {
    const node = document

    if (!node) return

    node.removeEventListener('keydown', this.onKeyDown.bind(this))
  }

  render() {
    return this.props.children
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
