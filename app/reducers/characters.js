// @flow
import { ADD_CHARACTER, SAVE_PROFILE } from '../actions/characters'
import { fromJS } from 'immutable'
import Character from '../models/Character'
import manAvatar from '../assets/images/male3-512.png'
import boyAvatar from '../assets/images/boy-512.png'
import femaleAvatar from '../assets/images/female1-512.png'

const jane = new Character({
  name: 'Jane Doe',
  avatar: femaleAvatar,
  color: '#FFE5E5',
  bio: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
  age: 20,
  gender: 'Female',
  bloodType: 'A',
  favoriteMusic: 'punk, hiphop, rock&roll',
  personality: 'quiet, shy'
})

const john = new Character({
  name: 'John Doe',
  avatar: manAvatar,
  color: '#B7EFFF',
  bio: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
  age: 26,
  gender: 'Male',
  bloodType: 'B',
  favoriteMusic: 'jazz, blues',
  personality: 'gentle, introverted, quiet'
})

const owen = new Character({
  name: "Owen Smith",
  avatar: boyAvatar,
  color: '#DDFFCE',
  bio: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
  age: 23,
  gender: 'Male',
  bloodType: 'O',
  favoriteMusic: 'hiphop, rock&roll, rap',
  personality: 'active, outgoing, extroverted'
})

const defaultState = fromJS([jane, john, owen])

export default function characters(state=defaultState, action: Object) {
  switch (action.type) {
    case ADD_CHARACTER:
      let c = action.payload
      return state.push(new Character(action.payload))
    case SAVE_PROFILE:
      const { characterId, formData } = action.payload
      const index = state.findIndex(c => c.get('id') === characterId)
      return state.set(index, fromJS(formData))
    default:
      return state
  }
}
