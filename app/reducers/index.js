// @flow
import { combineReducers } from 'redux'
import { routerReducer as routing } from 'react-router-redux'
import editor from './editor'
import ideas from './idea'
import characters from './characters'

const rootReducer = combineReducers({
  editor,
  ideas,
  characters,
  routing
})

export default rootReducer
