// @flow
import { SET_EDITOR_STATE, SET_SUGGESTION_LIST, TOGGLE_HELP } from '../actions/editor';
import {REHYDRATE} from 'redux-persist/constants'
import { Map, List, fromJS } from 'immutable'
import { EditorState } from 'draft-js'

const defaultState = Map({
  editorState: EditorState.createEmpty(),
  suggestions: List(),
  isModalVisible: false
})

export default function editor(state: Map=defaultState, action: Object) {
  switch (action.type) {
    case SET_EDITOR_STATE:
      return state.set('editorState', action.payload)
    case SET_SUGGESTION_LIST:
      return state.set('suggestions', action.payload)
    case TOGGLE_HELP:
      return state.set('isModalVisible', !state.get('isModalVisible'))
    case REHYDRATE:
      if (!action.payload.editor) return state
      const newState = action.payload.editor
      const editorState = EditorState.push(state.get('editorState'), newState.get('editorState').getCurrentContent(), 'update-state')

      state = state.set('editorState', editorState)
      state = state.set('suggestions', newState.get('suggestions'))
      state = state.set('isModalVisible', newState.get('isModalVisible'))
      return state
    default:
      return state
  }
}
