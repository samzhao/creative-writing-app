// @flow
import { ADD_IDEA } from '../actions/idea'
import { Map, List } from 'immutable'

const defaultState = List([
  '“All good books are alike in that they are truer than if they had really happened and after you are finished reading one you will feel that all that happened to you and afterwards it all belongs to you: the good and the bad, the ecstasy, the remorse and sorrow, the people and the places and how the weather was.”',
  "The sky has its way with me. As clouds lower their shoulders against the horizon, a warm front’s humid body slides along my skin, lifting the hem of my dress to curl around my waist and stretch along my spine.",
  "The atmosphere outside mirrors each tiny joint bubble inside me; the fates of both worlds have been permanently altered.",
  "The atmospheric and the arthritic trace tendrils of smoke from the industrial explosion.",
  "I and this pain-shadow lie on the couch. We turn in tandem under a blanket as mare’s tail clouds loop above me, against the icy blue."
])

export default function ideas(state=defaultState, action: Object) {
  switch (action.type) {
    case ADD_IDEA:
      return state.push(action.payload)
    default:
      return state;
  }
}
