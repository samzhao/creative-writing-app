// @flow
export const SET_EDITOR_STATE = 'SET_EDITOR_STATE'
export const SET_SUGGESTION_LIST = 'SET_SUGGESTION_LIST'
export const TOGGLE_HELP = 'TOGGLE_HELP'

export function setEditorState(editorState) {
  return {
    type: SET_EDITOR_STATE,
    payload: editorState
  }
}

export function setSuggestionList(suggestions) {
  return {
    type: SET_SUGGESTION_LIST,
    payload: suggestions
  }
}

export function toggleHelp() {
  return {
    type: TOGGLE_HELP
  }
}
