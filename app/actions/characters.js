// @flow
export const ADD_CHARACTER = 'ADD_CHARACTER'
export const SAVE_PROFILE = 'SAVE_PROFILE'

export function addCharacter(character) {
  return {
    type: ADD_CHARACTER,
    payload: character
  }
}

export function saveProfile(profile) {
  return {
    type: SAVE_PROFILE,
    payload: profile
  }
}
