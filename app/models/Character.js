import { Record } from 'immutable'
import uniqueId from 'lodash/uniqueId'
import unnamed from '../assets/images/unnamed-512.png'
import randomColor from 'randomColor'

const characterRecord = Record({
  id: '',
  link: '',
  color: randomColor({ luminosity: 'light' }),
  avatar: unnamed,
  name: 'Unnamed',
  bio: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus, ad.',
  age: 20,
  gender: 'Male',
  bloodType: 'A',
  favoriteMusic: '',
  personality: ''
})

export default class Character extends characterRecord {
  constructor(obj) {
    const id = uniqueId('c_')
    const character = Object.assign({
      id, link: `#/characters/${id}`
    }, obj)
    super(character)
  }
}
