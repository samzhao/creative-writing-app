import React, { Component, PropTypes } from 'react'
import autobind from 'autobind-decorator'
import Form from './Form'

const properties = {
  name: {type: 'string', title: 'Name'},
  bio: {type: 'string', title: 'Short Bio'},
  age: {type: 'number', title: 'Age'},
  avatar: {type: 'string', title: 'Avatar'},
  gender: {
    type: 'string',
    title: 'Gender',
    enum: ['Male', 'Female'],
    default: 'male'
  },
  bloodType: {
    type: 'string',
    title: 'Blood Type',
    enum: ['A', 'B', 'AB', 'O'],
    default: 'A'
  },
  favoriteMusic: {
    type: 'string',
    title: 'Favorite Music'
  },
  personality: {
    type: 'string',
    title: 'Personality'
  }
}
const uiProperties = {
  bio: {
    'ui:widget': 'textarea'
  },
  age: {
    'ui:widget': 'updown'
  },
  avatar: {
    'ui:widget': 'file'
  },
  favoriteMusic: {
    'ui:options': {
      addable: true
    }
  },
  favoriteMusic: {
    'ui:help': 'Use commas to separate them'
  },
  personality: {
    'ui:help': 'Use commas to separate them'
  }
}

export default class CharacterProfile extends Component {
  @autobind
  onSubmit({ formData }) {
    const { onSave, saveProfile, character } = this.props
    saveProfile({
      characterId: character.id,
      formData
    })
    onSave()
  }

  @autobind
  onChange({ formData }) {
    const { saveProfile, character } = this.props
    saveProfile({
      characterId: character.id,
      formData
    })
  }

  render() {
    const { character } = this.props
    if (!character) return

    return (
      <Form onSubmit={this.onSubmit}
            onChange={this.onChange}
            properties={properties}
            formData={character}
            uiProperties={uiProperties}>
        <div>
          <button type="submit" className="btn btn-save">Save</button>
        </div>
      </Form>
    )
  }
}
