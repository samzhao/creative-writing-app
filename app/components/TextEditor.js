// @flow
import React, { Component, PropTypes } from 'react'
import ReactDOM from 'react-dom'
import { Link } from 'react-router'
import autobind from 'autobind-decorator'
import { Map } from 'immutable'
import Editor from 'draft-js-plugins-editor'
import Mentions, { plugins } from './Mentions'
import { defaultSuggestionsFilter } from 'draft-js-mention-plugin'
import { EditorState, Modifier, RichUtils } from 'draft-js'
import DraggableBlock from './DraggableBlock'
import styles from './TextEditor.scss'

const getBlockRendererFn = (getEditorState, onChange) => (block) => {
  const type = block.getType()

  if (type == 'unstyled') return {
    component: DraggableBlock,
    props: {
      getEditorState
    }
  }
}

export default class TextEditor extends Component {
  constructor(props) {
    super(props)

    this.blockRendererFn = getBlockRendererFn(() => this.props.editor.get('editorState'), this.onChange)
  }

  componentDidMount() {
    this.focus()
  }

  @autobind
  focus() {
    if (!this.editor) return
    setTimeout(this.editor.focus)
  }

  @autobind
  onChange(editorState) {
    const { setEditorState } = this.props

    setEditorState(editorState)
  }

  @autobind
  handleTab(e) {
    e.preventDefault()

    const { editor } = this.props

    let currentState = editor.get('editorState')
    let newContentState = Modifier.replaceText(
      currentState.getCurrentContent(),
      currentState.getSelection(),
      '\u00a0\u00a0\u00a0'
    )

    this.onChange(EditorState.push(currentState, newContentState, 'insert-characters'))
  }

  @autobind
  handleKeyCommand(command) {
    const { editor } = this.props
    const newState = RichUtils.handleKeyCommand(editor.get('editorState'), command)

    if (newState) {
      this.onChange(newState)
      return 'handled'
    }
    return 'not-handled'
  }

  @autobind
  onSearchChange({value}) {
    const { setSuggestionList, characters } = this.props
    setSuggestionList(defaultSuggestionsFilter(value, characters))
  }

  render() {
    const { editor, characters, router } = this.props
    const editorState = editor.get('editorState')

    let suggestionList = editor.get('suggestions')

    if (suggestionList.size < 1) {
      suggestionList = characters
    }

    return (
      <div className={styles.editor} onClick={this.focus}>
        <Editor
          ref={el => {this.editor = el}}
          editorState={editorState}
          onChange={this.onChange}

          blockRendererFn={this.blockRendererFn}
          spellCheck={true}

          handleKeyCommand={this.handleKeyCommand}
          handleReturn={this.handleReturn}
          onTab={this.handleTab}

          //start for mention suggestion entries
          onUpArrow={() => this.forceUpdate()}
          onDownArrow={() => this.forceUpdate()}
          //end for mention suggestion entries

          plugins={plugins} />

        <Mentions
          onSearchChange={this.onSearchChange}
          suggestions={suggestionList}
        />
      </div>
    )
  }
}
