import React, { Component } from 'react'
import { Link } from 'react-router'
import autobind from 'autobind-decorator'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'
import { RouteTransition } from 'react-router-transition'

export default class Characters extends Component {
  @autobind
  addCharacter(e) {
    e.preventDefault()

    const { addCharacter } = this.props
    addCharacter({})
  }

  render() {
    const { characters, children, location } = this.props
    const path = location.pathname

    const list = (
      <div className="character-listing">
        <h3 className="mb-2">Characters</h3>
        <div className="row">
          {
            characters.map((char, idx) => (
              <div key={idx} className="col-xs-6 mb-1">
                <a href={`#/characters/${char.get('id')}`}>
                  <div className="card">
                    <img className="img-fluid" src={char.get('avatar')} alt=""/>
                    <div className="card-block">
                      <h5 className="card-body text-xs-center mt-2">
                        {char.get('name')}
                      </h5>
                    </div>
                  </div>
                </a>
              </div>
            )).toArray()
          }
          <div className="col-xs-6 mb-1">
            <a href="#" onClick={this.addCharacter}>
              <div className="card add-new">
                <i className="ion ion-md-add"></i>
              </div>
            </a>
          </div>
        </div>
      </div>
    )

    return (
      <div className="panel-body">
        <RouteTransition
          pathname={path}
          atEnter={{ opacity: 0 }}
          atLeave={{ opacity: 2 }}
          atActive={{ opacity: 1 }}
          mapStyles={styles => {
            if(styles.opacity > 1){
              return { display: 'none'}
            }
            return { opacity: styles.opacity}
          }}
        >
        { children ? children : list }
        </RouteTransition>
      </div>
    )
  }
}
