import React, { Component } from 'react'
import { Link } from 'react-router'
import autobind from 'autobind-decorator'
import { Entity } from 'draft-js'
import CharacterProfile from './CharacterProfile'

export default class Characters extends Component {
  constructor(props) {
    super(props)

    this.state = {
      mode: 'view'
    }
  }

  componentWillMount() {
    this.getCharacter(this.props)
  }

  componentWillUpdate(nextProps, nextState) {
    this.getCharacter(nextProps)
  }

  @autobind
  getCharacter(props) {
    const { characters, routeParams } = props

    if (!routeParams.id) return

    this.character = characters.find(c => c.get('id') === routeParams.id)

    const co = document.querySelectorAll('.character-occurrence')

    this.occurrences = [].map.call(co, o => {
      let name = o.getAttribute('name')
      name = (name !== null && name !== '') ? name.split('-')[1] : null
      if (this.character.get('id') === name)
        return o
    }).filter(item => item !== undefined)
  }

  render() {
    const { characters } = this.props
    const { mode } = this.state
    const c = this.character ? this.character.toJS() : null

    if (!c) {
      return (
        <div className="character-profile">
          <div className="back-btn">
            <Link to="characters">
              <i className="ion ion-md-arrow-round-back"></i>
            </Link>
          </div>

          <div className="panel-heading text-xs-center">
            <strong>Oops! Something went wrong :(</strong>
          </div>
        </div>
      )
    }

    const {
      name,
      bio,
      age,
      gender,
      bloodType,
      favoriteMusic,
      personality
    } = c

    let occurrences = null

    if (this.occurrences.length > 0) {
      occurrences = (
        <section className="text-xs-center">
          <h4>Mentions</h4>
          <div className="list-group occurrence-list">
            {
              this.occurrences.map((o, i) => (
                <a key={i} className="list-group-item" href="#" onClick={(e) => {
                  e.preventDefault()

                  o.scrollIntoView({
                    behavior: 'smooth',
                    block: 'end'
                  })
                }}>Mention #{i+1}</a>
              ))
            }
          </div>
        </section>
      )
    }

    const detailedInfo = (
      <div className="detailed-info mt-2 mb-2">
        <div className="row">
          <div className="col-xs-6 text-xs-right"><strong>Age:</strong></div>
          <div className="col-xs-6 text-xs-left">{age}</div>
        </div>
        <div className="row">
          <div className="col-xs-6 text-xs-right"><strong>Gender:</strong></div>
          <div className="col-xs-6 text-xs-left">{gender}</div>
        </div>
        <div className="row">
          <div className="col-xs-6 text-xs-right"><strong>Blood Type:</strong></div>
          <div className="col-xs-6 text-xs-left">{bloodType}</div>
        </div>
        <div className="row">
          <div className="col-xs-6 text-xs-right"><strong>Favorite Music:</strong></div>
          <div className="col-xs-6 text-xs-left">{favoriteMusic}</div>
        </div>
        <div className="row">
          <div className="col-xs-6 text-xs-right"><strong>Personality:</strong></div>
          <div className="col-xs-6 text-xs-left">{personality}</div>
        </div>
      </div>
    )

    return (
      <div className="character-profile">
        <div className="back-btn">
          <Link to="characters">
            <i className="ion ion-md-arrow-round-back"></i>
          </Link>
        </div>

        <div className="panel-heading text-xs-center">
          <div className="character-avatar">
            <img src={c.avatar} className="img-fluid img-thumbnail rounded-circle mb-1" style={{
              maxWidth: '35%',
              minWidth: '60px'
            }}/>
          </div>
          <h3 className="mt-1">
            {name}
          </h3>

          <p className="text-muted" style={{
            maxWidth: '80%',
            margin: '0 auto'
          }}>
            {bio}
          </p>
          { mode === 'view' && detailedInfo }
          { mode === 'view' && <button className="btn btn-save mt-2" onClick={() => this.setState({mode: 'edit'})}>Edit</button> }
        </div>

        {mode === 'edit' ? <CharacterProfile character={c} onSave={() => {this.setState({mode: 'view'})}} {...this.props} /> : occurrences}
      </div>
    )
  }
}
