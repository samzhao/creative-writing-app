// @flow
import React, { Component, PropTypes } from 'react'
import { Link } from 'react-router'
import { push } from 'react-router-redux'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'

import styles from './Home.scss'
import TextEditor from './TextEditor'

export default class Home extends Component {
  render() {
    const { children, location, router, editor, toggleHelp } = this.props
    let path = location.pathname.split('/')[1] || location.pathname
    const isModalVisible = editor.get('isModalVisible')

    if (path[0] !== '/') path = '/'+path

    let closeBtn = null

    if (children) {
      closeBtn = (
        <div className="panel-btns">
          <button type="button" onClick={() => router.push('/')}>
            <span>&times;</span>
          </button>
        </div>
      )
    }

    return (
      <div className="content">
        <div className={"modal" + (isModalVisible ? " active" : "") } onClick={toggleHelp}>
          <div className="modal-inner" onClick={(e) => e.stopPropagation()}>
            <h5 className="mb-1">About</h5>

            <p className="mb-2">
              Creative writers come across many difficulties when writing, including writer’s block, keeping track of character information, and inefficiencies with current writing systems. Ideas can be fleeting - the slightest delay or fumbling with the UI may lead to the loss of an idea. This is why we designed this application to allow users to effortlessly express their ideas in a flexible fashion.
            </p>

            <h5 className="mb-1">Current Features</h5>

            <div className="row mb-0">
              <ul>
                <li>Plain editor to prevent overthinking</li>
                <li>Save and reuse ideas by dragging and dropping</li>
                <li>
                  Keep track of characters
                  <ul>
                    <li>
                      Mention characters in text with the <code>@</code> symbol
                    </li>
                    <li>
                      Two-way linking of character information without simple clicks
                    </li>
                  </ul>
                </li>
                <li>
                  Standard text styling keyboard shortcuts
                  <ul>
                    <li><b>Bold</b>: <kbd><kbd>cmd</kbd> + <kbd>b</kbd></kbd></li>
                    <li><i>Italicize</i>: <kbd><kbd>cmd</kbd> + <kbd>i</kbd></kbd></li>
                    <li><u>Underline</u>: <kbd><kbd>cmd</kbd> + <kbd>u</kbd></kbd></li>
                    <li>Hide/cancel current panel/popup/operation: <kbd>esc</kbd></li>
                  </ul>
                </li>
                <li>
                  Keyboard shortcuts for advanced users
                  <ul>
                    <li>
                      To call out the Ideas panel: <kbd><kbd>ctrl</kbd> + <kbd>i</kbd></kbd>
                    </li>
                    <li>
                      To call out the Characters panel: <kbd><kbd>ctrl</kbd> + <kbd>c</kbd></kbd>
                    </li>
                    <li>
                      Toggle this popup: <kbd><kbd>ctrl</kbd> + <kbd>/</kbd></kbd>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <ReactCSSTransitionGroup transitionName="slide" transitionEnterTimeout={500} transitionLeaveTimeout={300}>
          <div className="panel" key={path}>
            { closeBtn }
            { children }
          </div>
        </ReactCSSTransitionGroup>
        <div className="action-btns">
          <Link to={path == '/ideas' ? '/' : 'ideas'} className="btn">
            <i className="ion ion-ios-folder"/>
          </Link>
          <Link to={path == '/characters' ? '/' : 'characters'} className="btn">
            <i className="ion ion-ios-people"/>
          </Link>
          <a href="#" className="btn" onClick={(e) => {e.preventDefault(); toggleHelp()}}>
            <i className="ion ion-md-help"/>
          </a>
        </div>
        <TextEditor {...this.props} />
      </div>
    );
  }
}
