import React, { Component } from 'react'
import SchemaForm from 'react-jsonschema-form'
import noop from 'lodash/noop'

export default class Form extends Component {
  constructor(props) {
    super(props)

    const { title="", type="object", required=[], properties={} } = props

    this.schema = {
      title, type, required, properties
    }
  }

  render() {
    const { onChange=noop, onSubmit=noop, onError=noop, uiProperties={}, formData={}, children=null } = this.props

    return (
      <SchemaForm schema={this.schema}
            onChange={onChange}
            onSubmit={onSubmit}
            onError={onError}
            formData={formData}
            uiSchema={uiProperties}
      >
        { children }
      </SchemaForm>
    )
  }
}
