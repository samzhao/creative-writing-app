import React, { Component } from 'react'
import { EditorBlock } from 'draft-js'

export default class DraggableBlock extends Component {
  dragStart(e) {
    const { block } = this.props
    e.dataTransfer.setData('data', JSON.stringify({
      content: block.getText()
    }))
  }

  render() {
    return (
      <div draggable={true} onDragStart={this.dragStart.bind(this)}>
        <EditorBlock {...this.props} />
      </div>
    )
  }
}
