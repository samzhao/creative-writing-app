import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import autobind from 'autobind-decorator'
import { Modifier, EditorState } from 'draft-js'
import Truncate from 'react-truncate'
import SearchInput, {createFilter} from 'react-search-input'

export default class Ideas extends Component {
  constructor(props) {
    super(props)

    this.currentDragIndex = null
    this.state = {
      searchTerm: ''
    }
  }

  componentDidMount() {
    // to fix react-truncate bug where text isn't truncated on load unless browser resized
    setTimeout(() => window.dispatchEvent(new Event('resize')), 100)

    // search input styles
    const input = document.querySelector('.search-input input')
    if (!input) return

    input.className += ' form-control mb-2'
  }

  @autobind
  dragStart(e) {
    e.dataTransfer.dropEffect = 'move'
    const parentNode = e.target.parentNode
    const p = parentNode.parentNode
    const index = [].indexOf.call(p.childNodes, parentNode)
    this.currentDragIndex = index
  }

  @autobind
  drop(e) {
    const data = e.dataTransfer.getData('data')
    if (!data || data.trim() == "") return
    const content = JSON.parse(data).content.trim()
    this.props.actions.ideaActions.addIdea(content)
  }

  @autobind
  dragEnd(e) {
    const { ideas } = this.props

    if (this.currentDragIndex === null) return

    let currentState = this.props.editor.get('editorState')
    let newContentState = Modifier.replaceText(
      currentState.getCurrentContent(),
      currentState.getSelection(),
      ideas.get(this.currentDragIndex)
    )

    this.props.actions.editorActions.setEditorState(EditorState.push(currentState, newContentState, 'insert-characters'))
  }

  render() {
    const { ideas } = this.props
    const { searchTerm } = this.state

    const filteredIdeas = ideas.filter(createFilter(searchTerm))

    return (
      <div className="panel-body panel-ideas" onDragOver={(e) => e.preventDefault()} onDrop={this.drop}>
        <h3 className="mb-2">Ideas</h3>
        <SearchInput className="search-input" onChange={term => this.setState({searchTerm: term})} />
        <div className="row">
          {
            filteredIdeas.map((i, idx) => {
              if (filteredIdeas.size > 3) {
                return (
                  <div key={idx} className="col-xs-6 mb-1">
                    <div className="card" draggable={true} onDragStart={this.dragStart} onDragEnd={this.dragEnd}>
                      <Truncate lines={3} ellipsis='...'>
                      {i}
                      </Truncate>
                    </div>
                  </div>
                )
              } else {
                return (
                  <div key={idx} className="col-xs-12 mb-1">
                    <div className="card" draggable={true} onDragStart={this.dragStart} onDragEnd={this.dragEnd}>
                      <Truncate lines={3} ellipsis='...'>
                      {i}
                      </Truncate>
                    </div>
                  </div>
                )
              }
            }).toArray()
          }
        </div>
      </div>
    )
  }
}
