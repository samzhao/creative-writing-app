import React, { Component, Children, cloneElement } from 'react'
import { connect } from 'react-redux'
import createMentionPlugin from 'draft-js-mention-plugin'
import mentionsStyles from './mentionsStyles.scss'

class MentionComp extends Component {
  static contextTypes = {
    router: React.PropTypes.object.isRequired
  }
  render() {
    const { className, mention, children } = this.props
    const { router } = this.context
    let { id, link, name, color } = mention.toJS()

    return (
      <a href={link}
         className={`${className} character-occurrence`}
         name={`character-${id}`}
         style={{ background: color }}
         spellCheck={false}
         onClick={(e) => {
          e.preventDefault()

          if (link[0] === '#') link = link.slice(1, link.length)

          router.push(link)
        }}>
        {
          Children.map(children, child => cloneElement(child, {text: name}))
        }
      </a>
    )
  }
}

const mentionPlugin = createMentionPlugin({
  theme: mentionsStyles,
  // entityMutability: 'MUTABLE',
  mentionComponent: MentionComp
})
const Mentions = mentionPlugin.MentionSuggestions
export default Mentions
export const plugins = [mentionPlugin]
